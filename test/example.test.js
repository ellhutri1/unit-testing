const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {

    before(() => {
        // initalization
        // create objects... etc..
        console.log("Initialising tests.")
    });

    it("Addition of 1 and 2", () => {
        // Tests
        const result = mylib.add(1, 2);
        expect(result).equal(3, "1 + 2 is not 3, for some reason?");
    });

    it("Substraction of 33 and 3", () => {
        const result = mylib.subtract(33, 3);
        expect(result).equal(30, "33 - 3 is not 30, for some reason?");
    });

    it("Division of 10 and 2", () => {
        const result = mylib.divide(10, 2);
        expect(result).equal(5, "10 / 2 is not 5, for some reason?");
    });

    it("Multiplication of 5 and 5", () => {
        const result = mylib.multiply(5, 5);
        expect(result).equal(25, "5 * 5 is not 25, for some reason?");
    });

    after(() => {
        // Cleanup
        // For example: shutdown Express server.
        console.log("Testing completed!")
    })
}); 